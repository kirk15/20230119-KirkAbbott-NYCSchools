//
//  SchoolDetailViewModel.swift
//  20230119-KirkAbbott-NYCSchools
//
//  Created by Kirk Abbott on 1/20/23.
//

import Foundation
import Combine


class SchoolDetailViewModel {
    
    private let network: Network
    private let school: School
    private var subscriptions = Set<AnyCancellable>()
    
    @Published var schoolSATScores: SchoolSATScores?

    init(network: Network, school: School) {
        self.network = network
        self.school = school
    }
    
    func requestSATScores() {
        self.network.requestModel(request: Environment.schoolDetails(self.school.dbn).request)
            .receive(on: DispatchQueue.main)
            .sink { completion in
                print(completion)
            } receiveValue: { (schoolSAT: [SchoolSATScores]) in
                self.schoolSATScores = schoolSAT.first
            }.store(in: &self.subscriptions)
    }
    
    var schoolName: String {
        return self.school.schoolName
    }
    
    var schoolOverview: String {
        return self.school.overviewParagraph
    }
    

    var readingScore: String {
        return "Reading SAT: \(self.schoolSATScores?.readingScore ?? "N/A")"
    }
    
    var mathScore: String {
        return "Math SAT: \(self.schoolSATScores?.mathScore ?? "N/A")"
    }
    
    var writingScore: String {
        return "Writing SAT: \(self.schoolSATScores?.writingScore ?? "N/A")"
    }
}
