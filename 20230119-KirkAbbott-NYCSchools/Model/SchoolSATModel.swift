////
////  SchoolSATModel.swift
////  20230119-KirkAbbott-NYCSchools
////
////  Created by Kirk Abbott on 1/20/23.
////
//
//import Foundation
//
//struct SchoolSATScores: Decodable {
//    let dbn: String
//    let schoolName: String
//    let readingScore: String
//    let mathScore: String
//    let writingScore: String
//
//    enum CodingKeys: String, CodingKey {
//        case readingScore = "satCriticalReadingAvgScore"
//        case mathScore = "satMathAvgScore"
//        case writingScore = "satWritingAvgScore"
//        case dbn, schoolName
//    }
//}
