//
//  NetworkManager.swift
//  20230119-KirkAbbott-NYCSchools
//
//  Created by Kirk Abbott on 1/20/23.
//

import Foundation
import Combine

enum Environment {
    
    private struct NetworkPaths {
        static let schoolsPath = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        static let schoolSATPath = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }
    
    private enum RequestType: String {
        case get = "GET"
        case post = "POST"
    }
    
    case schools
    case schoolDetails(_ dbn: String)
    
    var request: URLRequest? {
        switch self {
        case .schools:
            guard let url = URL(string: NetworkPaths.schoolsPath) else { return nil }
            var request = URLRequest(url: url)
            request.httpMethod = RequestType.get.rawValue
            return request
        case .schoolDetails(let dbn):
            var components = URLComponents(string: NetworkPaths.schoolSATPath)
            let dbnQuery = URLQueryItem(name: "dbn", value: "\(dbn)")
            components?.queryItems = [dbnQuery]
            
            guard let url = components?.url else { return nil }
            var request = URLRequest(url: url)
            request.httpMethod = RequestType.get.rawValue
            return request
        }
    }
    
}

enum NetworkError: Error {
    case badRequest
    case badStatusCode(Int)
    case decodeError(Error)
    case other(Error)
}

protocol Network {
    func requestModel<T: Decodable>(request: URLRequest?) -> AnyPublisher<T, NetworkError>
}

class NetworkManager {
    
    let session: URLSession
    let decoder: JSONDecoder
    
    init(session: URLSession = URLSession.shared, decoder: JSONDecoder = JSONDecoder(), decodeStrategy: JSONDecoder.KeyDecodingStrategy? = .convertFromSnakeCase) {
        self.session = session
        if let strategy = decodeStrategy {
            decoder.keyDecodingStrategy = strategy
        }
        self.decoder = decoder
    }
    
}

extension NetworkManager: Network {
    
    func requestModel<T: Decodable>(request: URLRequest?) -> AnyPublisher<T, NetworkError> {
        
        guard let request = request else {
            return Fail(error: NetworkError.badRequest).eraseToAnyPublisher()
        }
        
        return self.session.dataTaskPublisher(for: request)
            .tryMap { payload in
                if let httpResponse = payload.response as? HTTPURLResponse,
                   !(200..<300).contains(httpResponse.statusCode) {
                    throw NetworkError.badStatusCode(httpResponse.statusCode)
                }
                return payload.data
            }
            .decode(type: T.self, decoder: self.decoder)
            .mapError { error in
                if let decodeErr = error as? DecodingError {
                    return NetworkError.decodeError(decodeErr)
                }
                return NetworkError.other(error)
            }.eraseToAnyPublisher()
    }
    
}
