//
//  SchoolDetailController.swift
//  20230119-KirkAbbott-NYCSchools
//
//  Created by Kirk Abbott on 1/20/23.
//

import UIKit
import Combine

class SchoolDetailController: UIViewController {

    private let network: Network
    private let school: School
    
    lazy var schoolInfoView: SchoolInfoView = {
        let view = SchoolInfoView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let schoolDetailViewModel: SchoolDetailViewModel
    private var subs = Set<AnyCancellable>()
    
    init(viewModel: SchoolDetailViewModel) {
        self.schoolDetailViewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white

        let vStack = UIStackView(frame: .zero)
        vStack.translatesAutoresizingMaskIntoConstraints = false
        vStack.axis = .vertical
        vStack.spacing = 8
        
        vStack.addArrangedSubview(self.schoolInfoView)
        
        self.view.addSubview(vStack)
        vStack.bindToSuper()
        
        
        self.SchoolDetailController.$schoolSATScores
            .delay(for: 0.1, scheduler: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.schoolInfoView.configure(schoolDetailViewModel: self?.schoolDetailViewModel)
//                self?.schoolMapView.configure(schoolDetailViewModel: self?.schoolDetailViewModel)
            }
            .store(in: &self.subs)
        
        
        self.schoolDetailViewModel.requestSATScores()
    }
    


}
