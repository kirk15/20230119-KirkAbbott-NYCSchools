//
//  School.swift
//  20230119-KirkAbbott-NYCSchools
//
//  Created by Kirk Abbott on 1/20/23.
//

import Foundation

struct School: Decodable {
    let dbn: String
    let schoolName: String
    let overviewParagraph: String
}
