//
//  SchoolTableViewCell.swift
//  20230119-KirkAbbott-NYCSchools
//
//  Created by Kirk Abbott on 1/20/23.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {

    static let reuseID = "\(SchoolTableViewCell.self)"
    
    lazy var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Title"
        label.font = label.font.withSize(30)
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    
    lazy var overviewLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Overview"
        label.numberOfLines = 3
        label.textAlignment = .left
        return label
    }()
    
    var schoolDetailViewModel: SchoolDetailViewModel?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createUI() {
        let hStack = UIStackView(frame: .zero)
        hStack.translatesAutoresizingMaskIntoConstraints = false
        hStack.axis = .horizontal
        
        hStack.addArrangedSubview(self.titleLabel)
        
        let vStack = UIStackView(frame: .zero)
        vStack.translatesAutoresizingMaskIntoConstraints = false
        vStack.axis = .vertical
        vStack.spacing = 8
        
        vStack.addArrangedSubview(hStack)
        vStack.addArrangedSubview(self.overviewLabel)
        
        self.contentView.addSubview(vStack)
        vStack.bindToSuper()
    }
    
    func configure(schoolDetailViewModel: SchoolDetailViewModel?) {
        self.schoolDetailViewModel = schoolDetailViewModel
        
        self.titleLabel.text = schoolDetailViewModel?.schoolName
        self.overviewLabel.text = schoolDetailViewModel?.schoolOverview
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.overviewLabel.text = "Overview"
    }

}
